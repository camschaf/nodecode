const express = require('express'),
  router = express.Router(),
  // import the calculator module.
  calculator = require('../public/javascripts/calculator').calculate,
  history = require('../public/javascripts/history').history();

// GET index page
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// an array of all the pages.
// used to reduce repeated code.
[
  {'path':'/home1','page':'ui'},
  {'path':'/home2','page':'ui2'},
  {'path':'/home3','page':'ui3'},
].forEach(link => {
  router.get(link.path, (req,res) => {
    res.render(link.page);
  });
});

// post method to the path /calculate.
router.post('/calculate', ({body} = {},res) => {
  const equation = JSON.parse(body.equation),
    answer = calculator[equation.operator](Number(equation.nums.x),Number(equation.nums.y)),
    new_hist = `${equation.nums.x} ${equation.operator} ${equation.nums.y} = ${answer}`;
  history.addHistory(new_hist)
  console.log(new_hist)
  res.send({'operator':equation.operator, 'answer':answer})
})

router.delete('/deletestuff', (req,res) => {
  console.log('deleting stuff now')
  res.send({'message':'deletion complete'})
})

// Retrieve the calculator's history
router.get('/history', function(req, res, next) {
  res.send({ 'History': history.getHistory() })
  console.log(history.getHistory())
})

// send back error page on invalid request
router.all('*', function(req, res, next) {
  console.log('404 error page')
});

module.exports = router;
