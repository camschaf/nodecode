/*
* Module & Middleware definitions.
* createError   > allows the creation of simple http errors. (404, 500, etc)
* express       > the framework that is being used to generate this application.
* path          > allows the setting of the root path.
* cookieParser  > allows Express to interact with the cookies stored by the browser.
* logger        > http request logger that logs all requests to the node app.
*/
const createError = require('http-errors'),
  express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  logger = require('morgan');

//routes that will be used by this node app.
const indexRouter = require('./routes/index'),
  app = express();

//view engine setup. Extra reading on https://ejs.co/ for ejs file format.
app.set('views', path.join(__dirname, 'views'));  //where are the views that will be generated.
app.set('view engine', 'ejs');    //what format are those files.

//module & middleware setup.
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//router setup.
app.use('/', indexRouter);

//catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//error handler
app.use(function(err, req, res, next) {
  //set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  //render the error page
  res.status(err.status || 500);
  res.render('error');
});
//extra reading on the event loop.
//https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/

module.exports = app;
