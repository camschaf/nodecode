'use strict'

/**
 * Function that is in charge of managing the calculators history through 
 * variables stored in memory.
 * @module
 */
const history = () => {
    //this variable, once initialised, will be stored in memory for the current session.
    let calc_history = []

    /**
     * @returns the calculator's history.
     */
    const getHistory = () => {
        return calc_history
    }

    /**
     * @param {Array} new_calculation the latest calculation that was completed.
     */
    const addHistory = (new_calculation) => {
        calc_history.push(new_calculation)
    }

    return {
        getHistory, addHistory
    }
}

module.exports = { history }