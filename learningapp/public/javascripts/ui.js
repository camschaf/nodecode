'use strict'

/**
 * setup the button functionality using
 * jquery.
 */
const buttonSetup = () => {
    //the events that will be binded to the input fields.
    const events = 'change keyup paste'
    
    //addition checks
    $(`[id*='addition']`).on(events,() => {
        updateEquation({'x':$(`#addition-x`).val(), 
                        'y':$(`#addition-y`).val()},'+')
    })
    
    //subtraction checks
    $(`[id*='subtraction']`).on(events,() => {
        updateEquation({'x':$(`#subtraction-x`).val(), 
                        'y':$(`#subtraction-y`).val()},'-')
    })

    //multiplication checks
    $(`[id*='multiplication']`).on(events,() => {
        updateEquation({'x':$(`#multiplication-x`).val(), 
                        'y':$(`#multiplication-y`).val()},'*')
    })

    //division checks.
    $(`[id*='division']`).on(events,() => {
        updateEquation({'x':$(`#division-x`).val(), 
                        'y':$(`#division-y`).val()},'/')
    })

}
/**
 * displays the equation for each completed input.
 * @param {Object} num holds the x and y values for the equation.
 * @param {String} operator holds the operator for the equation.
 */
const updateEquation = (num, operator) =>{
    //check if both x and y are numbers and if the operator is correct.
    if(Number(num.x) && Number(num.y) && ['+','-','*','/'].includes(operator)){
        calculate[operator](Number(num.x), Number(num.y))
    }
}

/**
 * object that calculates the answer and then prints it out.
 */
const calculate = {
    '+': (x,y) => { const a = x+y; $('#addition-equals').text(a) },
    '-': (x,y) => { const b = x-y; $('#subtraction-equals').text(b) },
    '*': (x,y) => { const c = x*y; $('#multiplication-equals').text(c) },
    '/': (x,y) => { const d = x/y; $('#division-equals').text(d) }
}

/**
 * function used to bind the input box functionality once the page has loaded.
 */
$(document).ready(() => buttonSetup() )