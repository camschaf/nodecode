'use strict'

/**
 * object that calculates and returns the answer.
 * @module
 * @option '+' > takes x and y, then adds them.
 * @option '-' > ..., then minuses y from x.
 * @option '*' > ..., then multiplies them.
 * @option '/' > ..., then divides x by y.
 * @returns Number
 */
const calculate = {
    '+': (x,y) => x+y,
    '-': (x,y) => x-y,
    '*': (x,y) => x*y,
    '/': (x,y) => x/y
}

module.exports = { calculate }