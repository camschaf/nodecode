'use strict'

/* global Q */

/**
 * setup the button functionality using
 * jquery.
 */
const buttonSetup = () => {
    //the events that will be binded to the input fields.
    const events = 'change keyup paste'
    
    //addition checks
    $(`[id*='addition']`).on(events,() => {
        calculate({'x':$(`#addition-x`).val(), 
                        'y':$(`#addition-y`).val()},'+')
    })
    
    //subtraction checks
    $(`[id*='subtraction']`).on(events,() => {
        calculate({'x':$(`#subtraction-x`).val(), 
                        'y':$(`#subtraction-y`).val()},'-')
    })

    //multiplication checks
    $(`[id*='multiplication']`).on(events,() => {
        calculate({'x':$(`#multiplication-x`).val(), 
                        'y':$(`#multiplication-y`).val()},'*')
    })

    //division checks.
    $(`[id*='division']`).on(events,() => {
        calculate({'x':$(`#division-x`).val(), 
                        'y':$(`#division-y`).val()},'/')
    })

}
/**
 * calculates & displays the equation for each completed input.
 * @param {Object} num holds the x and y values for the equation.
 * @param {String} operator holds the operator for the equation.
 */
async function calculate (num, operator){
    //check if both x and y are numbers and if the operator is correct.
    if(Number(num.x) && Number(num.y) && ['+','-','*','/'].includes(operator)){
        const json = JSON.stringify({'nums':num, 'operator':operator})
        try{
            console.log('before async')
            const equationAnswer = await AJAX(json)
            if(equationAnswer){ console.log('async returned') }
            console.log('after async')
            
            displayAnswer[equationAnswer.operator](equationAnswer.answer);
        }catch(e){
            console.log('could not calculate answer')
        }
    }
}

const AJAX = data => {
    return Q(
        $.ajax({
            method: 'POST',
            url: '/calculate',
            data: { 'equation':data }
        })
    )
}

/**
 * this object contains functions that will print out the 
 * equation answers in their correct positions.
 */
const displayAnswer = {
    '+': a => $('#addition-equals').text(a),
    '-': b => $('#subtraction-equals').text(b),
    '*': c => $('#multiplication-equals').text(c),
    '/': d => $('#division-equals').text(d)
}

/**
 * function used to bind the input box functionality once the page has loaded.
 */
$(document).ready(() => buttonSetup() )